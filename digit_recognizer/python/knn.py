#!/usr/bin/python
"""
Digit Recognizer
Given: a training set matrix 42000x784 of handwritten images
Build: a knn classifer to predict images from the test set
"""
import numpy as np
import operator

def get_training_data(filename, header=True):
  lines = open(filename).readlines()
  labels = [] 
  training_matrix = []
  for line in lines:
    if header:
      header = False
      continue
    cols = line.strip().split(",")
    training_matrix.append(cols[1:])
    labels.append(cols[0])
  training_mat = np.matrix(training_matrix)
  training_mat = training_mat.astype(float)
  return labels, training_mat

def get_test_data(filename):
  return np.array([line.strip().split(",") for line in open("test.csv").readlines()[1:]]).astype(float)

def classify(input_vector, training_set, labels, k):
  m = training_set.shape[0]
  diff_mat = np.tile(input_vector, (m,1)) - training_set
  sq_diff_mat = np.multiply(diff_mat, diff_mat)
  sq_distances = sq_diff_mat.sum(axis=1)
  distances = np.sqrt(sq_distances)
  sorted_distance_indicies = distances.flatten().argsort().tolist()[0]
  class_count = {}
  for i in range(k):
    vote_label = labels[sorted_distance_indicies[i]]
    class_count.setdefault(vote_label, 0)
    class_count[vote_label] += 1
  sorted_class_count = sorted(class_count.iteritems(), key=operator.itemgetter(1), reverse=True)
  return sorted_class_count
