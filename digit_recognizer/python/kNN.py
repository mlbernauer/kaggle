#!/usr/bin/python

import numpy as np
import operator

# Function to create example dataset
def create_data_set():
  group = np.array([[1.0,1.1], [1.0,1.0], [0.0,0.0], [0.0, 0.1]])
  labels = ['A', 'A', 'B', 'B']
  return group, labels

# kNN algorithm
def kNN (input_vec, train_mat, labels, k):
  m = train_mat.shape[0]
  input_mat = np.tile(input_vec,(m,1))
  diff_mat = train_mat - input_mat
  sq_diff_mat = diff_mat**2
  sums = sq_diff_mat.sum(1)
  dist = sums**0.5
  # Sort distances ascending and get indicies
  sorted_indices = dist.argsort()
  label_counts = {}
  for i in range(k):
    label = labels[sorted_indices[i]]
    label_counts.setdefault(label,0)
    label_counts[label] += 1
  # Get sorted labels
  sorted_counts = sorted(label_counts.iteritems(), key=operator.itemgetter(1), reverse=True)
  return sorted_counts[0][0]

def normalize(data_mat):
  feature_min = data_mat.min(0)
  feature_max = data_mat.max(0)
  m = data_mat.shape[0]
  ranges = feature_max - feature_min
  norms = data_mat - np.tile(feature_min, (m,1))
  norms = norms / np.tile(ranges, (m,1))
  return norms, ranges, feature_min
