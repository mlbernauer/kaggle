#!/usr/bin/python
import kNN
import sys
import numpy as np

# Convert file to matrix
def file2mat(filename):
  lines = open(filename).read().strip().split("\r\n")
  lines = [i.split(",") for i in lines]
  lines.pop(0)
  data_mat = np.array(lines).astype(int)
  return data_mat

# Test the kNN Algorithm
def test_knn(k, iters):
  correct = 0.0
  total_trials = iters
  train_mat = file2mat("train.csv")
  test_mat = train_mat[0:total_trials]
  test_labs = test_mat[:,0]
  train_mat = train_mat[total_trials:]
  train_labs = train_mat[:,0]
  train_mat = np.delete(train_mat, 0,1)
  test_mat = np.delete(test_mat,0,1)
  m = test_mat.shape[0]
  for i in range(total_trials):
    idx = np.random.randint(m)
    input_vec = test_mat[idx]
    actual_label = test_labs[idx]
    predicted_label = kNN.kNN(input_vec, train_mat, train_labs, k)
    if actual_label == predicted_label:
      correct += 1
      print "Total correct: %d" % correct
  print "Total correct: %d" % correct
  print "Total trials: %d" % total_trials
  print "Percent correct: %f" % (correct/float(total_trials))

test_knn(int(sys.argv[1]), int(sys.argv[2]))
