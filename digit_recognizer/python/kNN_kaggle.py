#!/usr/bin/python
import kNN
import sys
import numpy as np


# Convert file to matrix
def file2mat(filename):
  lines = open(filename).read().strip().split("\r\n")
  lines = [i.split(",") for i in lines]
  lines.pop(0)
  data_mat = np.array(lines).astype(int)
  return data_mat

def run_knn(k):
  train_mat = file2mat("train.csv")
  test_mat = file2mat("test.csv")
  train_labs = train_mat[:,0]
  train_mat = np.delete(train_mat,0,1)
  m = test_mat.shape[0]
  print "ImageId,Label"
  for row in range(m):
    predicted = kNN.kNN(test_mat[row], train_mat, train_labs, k)
    print "%d,%d" % (row+1,predicted)

run_knn(int(sys.argv[1]))  
