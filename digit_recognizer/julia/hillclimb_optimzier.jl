#!/usr/bin/julia

# hill climb copies the theta vector, loops over each position and
# and increments at that position and decrements on that position
# this generates two neighbors for that position, this is repeated
# over every position in theta to generate its nearest neighbors
# These neighbors are then tested against a cost function. The
# best performing neighbor become theta and the process is repeated
# 'iters' times. By plotting the cost_hist curve, you can see this
# algorithm quickly gets stuck in local minima. Simulated annealing
# is a method that can be used to help find the global min.

function hill_climb_optimizer(y, X, hx, costf; iters=500, min=-10, max=10)
  m,n = size(X)
  theta = rand(min:max, n)
  best_cost = 1e10
  cost_hist = Float32[]
  for i in 1:iters
    neighbors = Array[]
    new_theta_inc = theta[:]
    new_theta_dec = theta[:]
    push!(neighbors, theta)
    for j in 1:length(theta)
      if new_theta_inc[j] < max
        new_theta_inc[j] += 1
        push!(neighbors, new_theta_inc)
      end
      if new_theta_dec[j] > min
        new_theta_dec[j] -= 1
        push!(neighbors, new_theta_dec)
      end
    end
    for j in neighbors 
      predictions = hx(X,j)
      cost = costf(y,predictions)

      if cost < best_cost
        theta = j
        best_cost = cost
      end
    end
   push!(cost_hist, best_cost)
  end
 (best_cost, cost_hist, theta)
end
    

