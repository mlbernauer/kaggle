# This function loads 'train'
# 'train_labs', 'test', 'test_labs'
# which are subsets from the digit
# dataset.
function load_data(filename)
  data = readcsv(filename)
  train = data[1:1000,2:end]
  train_labs = data[1:1000,1]
  test = data[1001:1500,2:end]
  test_labs = data[1001:1500,1]
  (train, train_labs, test, test_labs)
end
