#!/usr/bin/julia

function costf(y, X, theta, lambda)
  m,n = size(X)
  hx = X*theta
  gx = 1./(1 + exp(-hx))
  J = sum(((-y.*log10(gx) - (1-y).*log10(1-gx)))./m) + ((lambda)/(2*m)) * sum(theta[2:end].*theta[2:end]);
end 
