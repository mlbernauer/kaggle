#!/usr/bin/julia

# Genetic Optimizer
#
#
#
#

function genetic_optimizer(y, X, costf; min=-10, max=10, iters=500, popsize=50, step=1, mutprob=0.2, elite=0.2)
  m,n = size(X)

  # Mutation function
  function mutate(theta)
    # Mutation site
    i = rand(1:length(theta))
    dir = rand(-step:step)
    theta[i] += dir
    # Constrain vals to min and max
    if theta[i] < min theta[i] = min end
    if theta[i] > max theta[i] = max end
    theta
  end

  # Crossover function
  function crossover(theta1, theta2)
    # Crossover site
    i = rand(1:length(theta1))
    # Crossover operation
    [theta1[1:i]; theta2[i+1:end]]
  end

  # Create random populaton
  population = Array[]
  for i in 1:popsize
    vec = rand(min:max,n)
    push!(population, vec)
  end

  # Select elites 
  n_elite = round(elite*popsize)
  cost_hist = Float32[]
  top_solution = Int[]
  # Main loop
  for i in 1:iters
    # Score each solution
    scores = [costf(y,X,v) for v in population]
    sorted_idx = sortperm(scores)

    # Rank solutions
    ranked = population[sorted_idx]
    # Shrink populaton to top 2O% solutions
    population = ranked[1:n_elite]

    # Add mutated and bred forms of the winners
    while length(population) < popsize
      if rand() < mutprob
        # Mutate random soln from ranked solns
        k = rand(1:popsize)
        push!(population, mutate(ranked[k]))
      else
        # Crossover
        k = rand(1:popsize)
        j = rand(1:popsize)
        cross = crossover(ranked[k], ranked[j])
        push!(population, cross)
      end
      # Print the best scores
    end
    push!(cost_hist, scores[sorted_idx[1]])
    top_solution = ranked[sorted_idx[1]]
  end
  (cost_hist, top_solution)
end    
