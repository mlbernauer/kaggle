require("distance.jl")

# This is a kNN function that returns
# the k-nerest neighbors of a vector
# and predicts the unknown vecotr label
# using the mode label of the k neighbors

function kNN(train, labels, test; k=3)
  train_m, train_n = size(train)
  test_m, test_n = size(test)
  # Vector to store predictions
  predictions = zeros(test_m)
  for i in 1:test_m
    # Vecotr to store distnace between current test
    # example and all m training examples
    distances = zeros(train_m)
    for j in 1:train_m
      # Calculate euclidean distance
      distances[j] = euclidean_distance(test[i,:], train[j,:])
    end
    # Sort distances and use sorted index to 
    # retreive closest labels
    sorted_labs = labels[sortperm(distances)]
    # Return the k-closest neighbors
    nearest_neighbors = sorted_labs[1:k]
    # Dictionary to hold label counts
    counts = Dict()
    for l in nearest_neighbors
      if haskey(counts, l)
        # Update label count
        counts[l] += 1
      else
        # Add label if not exist
        counts[l] = 1
      end
    end
    # Sort the counts by descending freq
    sorted_counts = sort([(i[2], i[1]) for i in counts], rev=true)
    # Make prediction based on most freq label
    predictions[i] = sorted_counts[1][2]
  end
  # Returns prediction vector
  predictions
end
