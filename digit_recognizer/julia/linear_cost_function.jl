require("kNN.jl")

# Cost function to determine cost of
# associated with various weight vectors
# for use in the kNN algorithm.
#
# The original feature vector X of n length
# The parameter vector of weights theta of n length
# 
# Element wise multiplication of X .* theta
# results in weighting of features in X

# The cost function evaluates the accuracy of 
# the kNN classifier after applying the weights
# to the feature vector X

function cost_function(train, train_labs, test, test_labs; theta=ones(size(test,2)), k=5)
  test_new = test .* theta'
  predictions = kNN(train, train_labs, test_new, k=k)
  # Cost is defined as 1 - accuracy
  accuracy = mean(predictions .== test_labs)
  cost = 1 - accuracy
end
