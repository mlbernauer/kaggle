# Random optimization function
# optimizes parameters theta with
# respect to the cost function costf.
# Random optimization is not an efficient
# algorithm because it does not take advantage
# of good solutions previously seen

function random_optimize(y, X, hx, costf; iters=50, min=-10, max=10)
  m,n = size(X)
  best_cost = 1e10
  params = zeros(n)
  cost_history = Float32[]
  for i in 1:iters
    # Create random theta vector
    theta = rand(-50:50, n)
    # Create predictions based on theta
    predictions = hx(X, theta)
    # What is cost between actual and predicted
    cost = costf(y, predictions)
    # If lower cost, update best cost
    if cost < best_cost
      best_cost = cost
      # Save the theta to return to user
      params[1:end] = theta
    end
    # Save the cost history for monitoring convergence
    push!(cost_history, best_cost)
    # Exit if found perfect solution
    if best_cost == 0
      break
    end
  end
  # Return best cost, history, and found parameters
  (best_cost, cost_history, params)
end
    
