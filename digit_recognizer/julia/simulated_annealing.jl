#!/usr/bin/julia

# Simulated annealing optimization algorithm
# returns parameters theta which optimize hx
# with respect to some cost function which takes
# labels 'y' and predictions and computes the 
# cost (sum squared error) between predictions and label

function simulated_annealing(y, X, costf; T=1000, cool=0.95, step=1, min=-10, max=10)
  m,n = size(X)
  # Create random solution
  theta = rand(min:max, n)
  cost_hist = Float32[]
  while T > 0.1
    # Choose random parameter to change
    idx = rand(1:n)
    # Chose a step size and direction to mutate
    dir = rand(-step:step)
    # Perform muatation
    new_theta = theta[:]
    new_theta[idx] = dir
    # Constrain limits
    if new_theta[idx] < min new_theta[idx] = min end
    if new_theta[idx] > max new_theta[idx] = max end
    # Calculate and compare costs
    cost_old = costf(y, X, theta)
    cost_new = costf(y, X, new_theta)
    # Calculate probability of accepting the worst solution
    # Prob is lower if the cost is high
    prob = e^((-cost_new - cost_old)/T)
    if cost_new < cost_old || rand() < prob
      theta = new_theta[:]
      push!(cost_hist, cost_new)
    else
      push!(cost_hist, cost_old)
    end
    # Cool temp with rate 'cool'
    T *= cool
  end
  (cost_hist[end], cost_hist, theta)
end
