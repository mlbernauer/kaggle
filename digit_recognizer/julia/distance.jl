function euclidean_distance(x1, x2)
  sqrt(sum((x1 - x2).^2))
end
