#!/usr/bin/python
"""
Michael Bernauer
2/8/2014

This script tests the kNN algorithm for
hand writing recognition. 
"""
import kNN
import numpy as np
import os
import sys

# Convert image .txt file into Numpy vector
def image2vec(filename):
  lines = open(filename).read().strip().replace("\r\n", "")
  array = np.array(list(lines))
  array = array.astype(int)
  return array

# Build matrix from listing of files
def generate_mat(directory):
  m = len(os.listdir(directory))
  n = len(image2vec(directory+"/"+os.listdir(directory)[0]))
  data_mat = np.zeros((m,n))
  dir_files = os.listdir(directory)
  labels = []
  for i in range(m):
    label = dir_files[i].split("_")[0]
    labels.append(label)
    data_mat[i] = image2vec(directory+"/"+dir_files[i])
  return data_mat, labels

# Tests the kNN algorithm
def test_knn(k, iters):
  correct = 0.0
  total_runs = iters
  test_mat, test_labs = generate_mat("test_digits")
  train_mat, train_labs = generate_mat("training_digits")
  m = test_mat.shape[0]
  for i in range(total_runs):
    idx = np.random.randint(m)
    input_vec = test_mat[idx]
    actual_label = test_labs[idx]
    predicted_label = kNN.kNN(input_vec, train_mat, train_labs, k)
    if actual_label == predicted_label:
      correct += 1
      print "Total correct: %d" % correct
  print "Final results!"
  print "Total correct: %d" % correct
  print "Total trials: %d" % total_runs
  print "Percent correct: %f" % (100.0*(correct/float(total_runs)))

test_knn(int(sys.argv[1]), int(sys.argv[2]))  


